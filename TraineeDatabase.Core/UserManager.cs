﻿using System.Collections.Generic;
using System.Linq;

namespace TraineeDatabase.Core
{
    public class UserManager
    {
        public AuthenticationResult Authenticate(string loginName, string password)
        {
            var traineeDatabaseEntities = new TraineeDatabaseEntities();

            var result = new AuthenticationResult();
            result.IsAuthentic = false;

            var user = traineeDatabaseEntities.Users.SingleOrDefault(g => g.LoginName == loginName);

            if (user != null)
            {
                if (user.Password == password)
                {
                    result.User = user;
                    result.IsAuthentic = true;
                    result.Message = string.Empty;
                }
                else
                {
                    result.Message = "Invalid password.";                    
                }
            }
            else
            {
                result.Message = "Invalid login name.";
                
            }
            
            return result;
        }

        public User GetUser(int id)
        {
            var traineeDatabaseEntities = new TraineeDatabaseEntities();

            var user = traineeDatabaseEntities.Users.SingleOrDefault(g => g.Id == id);

            return user;
        }

        public List<User> GetUsers(bool isActiveOnly)
        {
            var traineeDatabaseEntities = new TraineeDatabaseEntities();

            List<User> users = isActiveOnly ? traineeDatabaseEntities.Users.Where(g => g.IsActive).ToList()
                : traineeDatabaseEntities.Users.ToList();

            return users;
        }

        public List<User> GetUsers(int groupId, bool isActiveOnly)
        {
            var traineeDatabaseEntities = new TraineeDatabaseEntities();

            List<User> users = isActiveOnly ? traineeDatabaseEntities.Users.Where(g => g.IsActive && g.UserGroupId == groupId).ToList()
                : traineeDatabaseEntities.Users.Where(g=> g.UserGroupId == groupId).ToList();

            return users;
        }
    }
}
