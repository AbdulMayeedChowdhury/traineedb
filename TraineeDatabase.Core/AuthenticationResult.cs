﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraineeDatabase.Core
{
    public class AuthenticationResult
    {
        public User User { get; set; }

        public bool IsAuthentic { get; set; }

        public string Message { get; set; }
    }
}
