﻿using System.Collections.Generic;
using System.Linq;

namespace TraineeDatabase.Core
{
    public class UserGroupManager
    {
        public UserGroup GetUserGroup(int id)
        {
            var traineeDatabaseEntities = new TraineeDatabaseEntities();

            var userGroup = traineeDatabaseEntities.UserGroups.SingleOrDefault(g => g.Id == id);

            return userGroup;
        }

        public List<UserGroup> GetUserGroups(bool isActiveOnly)
        {
            var traineeDatabaseEntities = new TraineeDatabaseEntities();

            List<UserGroup> userGroups = isActiveOnly ? traineeDatabaseEntities.UserGroups.Where(g => g.IsActive).ToList() 
                : traineeDatabaseEntities.UserGroups.ToList();

            return userGroups;
        }
    }
}
