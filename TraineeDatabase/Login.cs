﻿using System;
using System.Windows.Forms;
using GoTrexReportViewer;
using TraineeDatabase.Core;

namespace TraineeDatabase
{
    public partial class Login : Form
    {
        public bool IsLoginSuccessful { get; set; }

        public Login()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            IsLoginSuccessful = false;

            string loginName = txtLoginName.Text.Trim();
            string password = txtPassword.Text.Trim();

            if (loginName.Length == 0)
            {
                MessageBox.Show(@"Please Enter User ID.");
                txtLoginName.Focus();
                return;
            }

            if (password.Length == 0)
            {
                MessageBox.Show(@"Please Enter password.");
                txtPassword.Focus();
                return;
            }

            try
            {
                AuthenticationResult authResult = new UserManager().Authenticate(loginName, password);

                if (authResult.IsAuthentic)
                {
                    LoginInformation.UserId = authResult.User.Id;
                    LoginInformation.LoginName = authResult.User.LoginName;
                    LoginInformation.UserName = authResult.User.FullName;
                    LoginInformation.LoginTime = DateTime.Now;
                    IsLoginSuccessful = true;
                }
                else
                {
                    MessageBox.Show(@"Authentication failed. Please check your Login ID and Password.");
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Login failed. Error: " + ex.Message, @"Error");
                return;
            }

            Hide();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(@"Do you really want to cancel?", @"Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {
            txtLoginName.Text = @"Admin";
            txtPassword.Text = @"a"; 
        }
    }
}
