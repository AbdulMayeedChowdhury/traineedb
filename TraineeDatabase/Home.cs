﻿using System;
using System.Windows.Forms;
using GoTrexReportViewer;

namespace TraineeDatabase
{
    public partial class Home : Form
    {
        public Home()
        {
            InitializeComponent();
        }

        private void LoadForm(Form form)
        {
            form.ShowDialog();
        }

        private void Home_Load(object sender, EventArgs e)
        {
            Login loginForm = new Login();

            loginForm.ShowDialog();

            if(!loginForm.IsLoginSuccessful)
            { 
                MessageBox.Show(@"Login failed.");
                Application.Exit();
            }

            loginForm.Close();

            lblUserName.Text = LoginInformation.UserName;
            lblLoginName.Text = LoginInformation.LoginName;
            lblLoginTime.Text = LoginInformation.LoginTime.ToString("hh:mm:ss tt");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(@"Are you sure to exit?", @"Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if(result == DialogResult.Yes)
            {
                Application.Exit();
            }

            return;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //LoadForm(new frmAbout());
        }

        private void writeTestSpecToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadForm(new WriteTestSpec());
        }
    }
}
