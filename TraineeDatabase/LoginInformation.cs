﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoTrexReportViewer
{
    public class LoginInformation
    {
        public static string UserName { get; set; }

        public static int UserId { get; set; }

        public static string LoginName { get; set; }

        public static DateTime LoginTime { get; set; }

        public static string Organization { get; set; }
    }
}
